Parrainage de la Simplon Chambéry Promo #1 par ChambeCarnet
===========================================================

La soirée parrainage a eu lieu le 14 mars 2019 au mug. Le debrief proposé se déroule en deux parties:

* Les apprenant-es et les membres de l'équipe donnent leurs impressions avec trois entrées : tête, coeur, jambes
* Un debrief complémentaire avec les questions qui restent, plutôt orienté technique.
