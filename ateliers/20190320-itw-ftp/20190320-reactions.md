Rappel:
- mardi 18/03 : Pierre faits + sentiments de flottement
- 18/03 : interpellations des délégués sur différents points :
  - Objectifs pas clair
  - transmission en ligne sur les demandes de dérogation à l'oral
  - une majorité pas à l'aise avec la pédagogie (pb d'évaluation voir sentiment d'échec)
  - certains trouvent que stéphane ne va pas assez les voir
  - sentiment de dispersion
- mardi 25/03 : retour de l'équipe pédagogique sur l'interpellation

- pour dérogation : par mail (trace écrite) + pas à l'aise avec Justin
- pour objectif : autonomie, décrits dans le brief projet
- atelier scratch : fait partie du bloc "programmation" : mêmes notions abordées différemment

-> Les objectifs et les consignes sont bien données mais ne sont pas complètement reçues :
- améliorer la transmission de la part des formateurs
- améliorer la réception de la part des apprenant-es (organisation perso)

Sur l'évaluation : expliciter la façon de travailler :
- Comment on évalue les apprenant-es
- Pourquoi on va voir certain-e plutôt que d'autres (pas un hasard)
  -> Explique la différence d'approche Jérôme / Stéphane

1. Reprendre la base
un formateur ne peut pas passer 30 min avec chaque apprenant chaque jour

2. Comment on évalue : -> on ne l'a pas explicité
par ailleurs il y a d'autres moyens de suivre un apprenant qu'en regardant ce qu'il fait : comment il interagit avec les autres, quelles questions il pose, quelles questions il se pose

3. Comment on fonctionne -> on ne l'a pas explicité
Point hebdomadaire par apprenant-e pour savoir où il en est avec les éléments collectés par les deux formateurs. En fonction de ça on décide de suivre plus particulièrement certain-es

Sur la pédagogie :
Remettre en perspective la pédagogie pour éviter le sentiment de dispersion.

Exemple : sur sass : on demande de l'utiliser : on vous laisse la place pour l'utiliser, trouver d'autres moyens de l'utiliser.

Le projet n'est pas la finalité mais la création du besoin d'utiliser des outils. Suivant la façon dont avance le projet certain-es auront besoin de certains outils et d'autres non. Le projet est borné par :
- les objectifs
- les consignes

Si un apprenant sort du cadre on le remet dedans

Sur le fonctionnement global :
- compétences techniques
- insertion professionnelle

Compétence technique : savoir, savoir-faire, connaissance vs pratique
Les veilles, les ateliers (comme scratch), les exercices sont faits pour vous donnez des outils dont vous vous saisirez tout de suite dans le projet ou plus tard dans un autre projet. On ne peut pas utiliser un outil qu'on ne connaît pas. Par contre si on a un problème à résoudre, il faut :
- déjà pensé à ce qu'on sait
- si on ne trouve pas chercher à le résoudre
- mettre en pratique

Finalement :
La programmation n'est pas "facile" à acquérir et si vous l'obteniez en un claquement de doigt au bout de 3 semaines, ce que vous apprenez perdrait de la valeur :
- il faut bosser, pratiquer
- respecter les consignes
- chercher à résoudre les problèmes par soi-même et si vous êtes là c'est que vous en êtes capables
