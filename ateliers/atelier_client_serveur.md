Objectif : simuler les interactions entre un navigateur et un serveur

Outil : boite de tangram

1. par groupe les apprenants font un plan de leur tangram

2. ils font une analogie entre page html et tangram

3. simulation des requêtes avec des réponse 200, 400, 500

-> analogie html == plan
-> chaque piece == requête sur le serveur