Exercices sur les relations

# Step 1
Pensez à votre dentifrice
Quel goût a-t-il ?
Quelle marque ?

# Step 2
Pensez à un fruit
Comment s'appelle l'arbre qui produit ce fruit
A quelle famille appartient cet arbre ?
Quelle est ou quelles sont les couleurs de ce fruit ?

# Step 3
Prénom
Ville
Adresse
Ou vous allez faire vos courses ?
--> Account ?