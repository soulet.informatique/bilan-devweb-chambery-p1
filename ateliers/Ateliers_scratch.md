Ateliers scratch
================

Simplon Chambéry Promo #1

# Contexte
Le formateur n'a pas besoin de connaître Scratch.
Bien rappeler le contexte du pourquoi des ateliers

# Objectifs
1. Que les apprenant-es soient en situation de formateur et puissent faire passer des ateliers "scratch" à des collégiens / jeunes
2. Qu'ils puissent transmettre des notions d'algorithmie

# Atelier 1
## Etape 1 : 30/45 min
Allez sur scratch et amusez-vous

## Etape 2 : 45 min : par groupe de 4
Choisissez une des animations et préparer un atelier avec :
### Les objectifs
### Les supports
### Les consignes
### Mettre en évidence les notions d'algo vue sur les 3 semaines algo (variables, boucle, fonction, condition, tableau)

# Atelier 2
En reprenant les groupes de 4. Groupes A, B, C, D, E

## Etape 1
se diviser en :
- 2 aprenant-es
- 1 formateur-trice
- 1 observateur-trice
Les apprenant-es A, vont voir le formateur B avec l'observateur B
Les apprenant-es B, vont voir le formateur C avec l'observateur C
etc...

Pendant 35 min, le formateur fait passer son atelier aux apprenant-es, l'observateur observe.
Pendant 15 min on debrief pour trouver les points bloquants, les améliorations

## Etape 2
Même process :
- on inverse les couples apprenants / formateur-observateur
- les apprenants B vont voir le formateur A
-> Ceux.celles qui étaient en position de formateur.trice, se retrouvent en position d'apprenant-es avec les apprenant-es de la première étape

+ débrief

## Etape 3
Rédiger correctement les consignes en fonction des problèmes repérés
Penser au debrief final avec les apprenant-es
Penser à rappeler la problématique des notions d'algo
Bien penser à avoir les consignes écrites lors du vrai atelier

# Atelier 3
Le vrai atelier. Les apprenant-es sont tou.te.s en position de formateur.trice. Ils prennent leur ordi et ont 2 apprenant-es collégien-nes. Ils suivent les consignes qu'il.elle.s ont définies.




