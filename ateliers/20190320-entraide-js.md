Objectif :
Mettre en place un système d'entraide afin que ceux-celles qui sont en difficulté puissent avoir une autre approche que celle dispensée par les formateurs.

Dispositif pédagogique
Mettre par pair : un-e apprenant-e qui se sent en difficulté, un-e apprenant-e qui se sent à l'aise

Il y a deux axes :
- auto-évaluation : savoir où on en est
- combler les lacunes

Notions abordées :
- variable, fonction, boucle, condition, tableau

Chacun-e doit se positionner sur un niveau compris en 1 et 5 sur les 5 axes de travail. Chaque niveau doit être explicité par un problème concret qu'il n'a pas compris, pas résolu ou au contraire résolu, compris.

Chacun-e se positionne sur les 5 axes.

On prend 10 en niveau 1, si pas assez en niveau 2 etc... Et on les positionne avec 10 autres qui correspondent à leur problématique, ordre de traitement des notions :
1 variable
2. tableau
3. condition
4. fonction
5. condition

