Choix du framework
==================

Objectif : Apprendre à choisir une techno dans un contexte

Tâche : choisir le framework javascript que nous étudierons sur 3 semaines dans le cadre de la formation.

Modalités d'évaluation :

* Avoir réussi à choisir une tech
* Justifier son choix en explicitant ses critères de choix et en donnant les moyens utiliser pour choisir et conclure
* Avoir fait consensus dans le groupe
* Ré-évaluer son choix à mi-parcours lors de la phase implémentation

# Brief projet framework JS
Faire une interface responsive à partir d'une api tierce (api Keolis par exemple) qui devra comporter au moins :

* Manipulation du DOM virtuel
* Gestion du store des données

# Le timing
1. 15 min : critères et moyens
2. 1h30 : étude par groupe d'un framework. Attendu : des arguments avec critère et moyen + évaluation de la certitude
Pause
3. 30 min : appropriation des éléments d'un autre groupe
4. 30 min : vode de tendances
