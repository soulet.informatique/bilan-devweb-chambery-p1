<!-- TITLE: Git -->
<!-- SUBTITLE: Jeu de carte prise en main du workflow git -->

# Scénario 1
1. Récupérer les sources d'un projet existant
2. Ajouter un nouveau fichier au projet
3. Envoyer le nouveau fichier sur le dépôt distant

# Scénario 2
1. Récupérer les sources d'un projet existant
2. Créer une nouvelle branche pour ajouter une nouvelle fonctionnalité
3. Ajouter un fichier pour implémenter la nouvelle fonctionnalité
4. Fusionner la nouvelle branche à la branche principale
5. Envoyer le code sur le dépôt distant

# Scénario 3
1. Créer un nouveau projet Git
2. Ajouter un nouveau fichier au projet
3. Envoyer le nouveau fichier sur un dépôt distant

# Scénario 4
1. Récupérer les sources d'un projet existant
2. Récupérer le code source du projet distant sans fusionner avec le code source local
3. Fusionner le code source distant et le code source local

# Scénario 5
1. Récupérer les sources d'un projet existant
3. Ajouter un fichier pour implémenter une nouvelle fonctionnalité
4. Corriger un bug dans un fichier existant sans versionner la nouvelle fonctionnalité
5. Envoyer le code sur le dépôt distant permettant de corriger le bug
6. Récupérer le code en cours pour continuer d'implémenter la nouvelle fonctionnalité
