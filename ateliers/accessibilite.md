Accessibilité
=============

# Référentiel
[RGAA 3 2017](https://references.modernisation.gouv.fr/rgaa-accessibilite/)

# Les critères
[Voir la liste des critères](https://references.modernisation.gouv.fr/rgaa-accessibilite/criteres.html)

| Numéro      |     Libellé    |   Nombre |
|:-------------:|:-------------|---------:|
|1| Images | 10 |
|2| Cadres | 2 |
|3| Couleurs | 4 |
|4| Multimédia | 22 |
|5| Tableaux | 8 |
|6| Liens | 5 |
|7| Scripts | 5 |
|8| Éléments obligatoires | 10 |
|9| Structuration de l'information | 6 |
|10| Présentation de l'information | 1 |
|11| Formulaires | 15 |
|12| Navigation | 14 |
|13| Consultation | 17 |


## Première analyse des critères

Les apprenant.e.s se répartissent les tâches :

* Critrères 1 et 6
* Critères 9 et 3
* Critères 10 et 8
* Critères 12

Tous les sites devront satisfaire les critères A.
Une analyse poussée devra être fait sur les critères AA et AAA pour voir ce qu'il faudrait faire pour qu'ils soient 
satisfaisants