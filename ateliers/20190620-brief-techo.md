Objectif : Apprendre à choisir une techno dans un contexte

Tâche : choisir le framework javascript que nous étudierons sur 3 semaines dans le cadre de la formation.

Modalités d'évaluation :
* Avoir réussi à choisir une tech
* Justifier son choix en explicitant ses critères de choix et en donnant les moyens utiliser pour choisir et conclure
* Avoir fait consensus dans le groupe
* Ré-évaluer son choix à mi-parcours lors de la phase implémentation

# Brief projet framework JS
Faire une interface responsive à partir d'une api tierce (api Keolis par exemple) qui devra comporter au moins :
* Manipulation du DOM virtuel
* Gestion du store des données

# Le timing
1. 15 min : critères et moyens
2. 1h30 : étude par groupe d'un framework. Attendu : des arguments avec critère et moyen + évaluation de la certitude
Pause
3. 30 min : appropriation des éléments d'un autre groupe
4. 30 min : vode de tendances

# Le dispositif pédagogique

1. 15 min : tour de la promo : comment choisir une techno (critères et moyens) ?
critères : (marché pro, besoins users, demande client, critères techniques, ...)
moyens : (recherche web, veille entreprise, récoltes de retours d'exp, débats, bench techno, test, avantages / inconvénients

2. 1h30 : répartition en groupe de 10 par techno : quelle stratégie pour le groupe ? -> on essaie de répartir les groupes par motivation -> ceux par vue dans vue, ceux par react dans react et pourquoi pas un 3ème groupe Js natif ou autre proposition.... -> si on ne veut pas qu'il y ait le js natif, il faut imposer des conditions sur la techno des 3 semaines
-> restit 5 min + paper board

là où ils sont sûrs de leur coup, on sait pas...

pause (15 min)

3. 30min chgt de groupes : finaliser l'appréciation des critères
-> 5min + paper board (arguments positifs)

4. Vote par arguments positifs
+ Se positionner sur la techno :
* vert ok
* rouge : objection argumentée
* jaune : question de clarification
* bleu : laisse le groupe décider
//* violet : oui mais plus