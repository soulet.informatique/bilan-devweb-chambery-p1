Projet CV
=========

# Objectifs
* Se perfectionner en html/css
* Faire une maquette et développer l'interface du site à partir de la maquette
* Faire un site responsive en utilisant bootstrap
* Utiliser le gestionnaire de package pour faire du Sass

# Modalités d'animation
Ce projet est individuel. Il se déroulera sur 5 demi-journées.

## Les étapes :
* 1. Créer le contenu, la wireframe
* 2. Le planning sur les 5 jours, les technos qui seront utilisées
* 3. Mettre en oeuvre

# Repo git :
https://github.com/simplonco/Create-a-CV-with-Bootstrap-to-conquer-the-World
