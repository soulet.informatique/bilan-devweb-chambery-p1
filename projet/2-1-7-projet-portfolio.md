Project PortFolio

L'objectif est de faire un portfolio sous symfony avec une base mysql.

Durée: 1 semaine et demie
Projet individuel

# Objectifs:
* Découvrir Symfony (Controller, twig, formulaires, entités, router, sécurité)
* Créer une base, un schéma, créer les composants pour interagir en PHP, Créer un début de back-end
* Avoir une base pour un port-folio
* Mettre en ligne un projet symfony

# Compétences
* Compétence 5 : créer une base de données, niveau imiter
* Compétence 6 : développer les composants d'accès au données, niveau imiter
* Compétence 7 :  Développer la partie back-end d'une application web, niveau imiter

# Description
Un site Portfolio est une collection de projets réalisées décrivant et illustrant l’apprentissage ou la carrière d’une personne, son expérience et ses réussites.

Dans le monde numérique actuel, un site portfolio et gitlab sont sans doute plus important qu'un CV, quel que soit le secteur dans lequel vous travaillez.

Dans ce projet vous allez créer un site Portfolio pour afficher les différents projets que vous avez réalisé en donnant le lien gitlab de chaque projet.

* Créer un projet Symfony et coder l'application avec ce framework
* Contraintes :
** Pas de css -> L'objectif est d'avoir l'ensemble BDD/HTML fonctionnel
** Tous les noms de variable, fonctions, commentaires doivent être en anglais
