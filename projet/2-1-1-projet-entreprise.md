Projet Entreprise
=================

# Mise en place
## Objectifs

* Travailler en groupe
* Découvrir toutes les technos qui seront vues au cours de la formation
* Utiliser une méthode que chaque apprenant.e aura réellement mis la main dans un bout de code html, css, js, php, sql
* Utiliser git

## Supports

Définir étapes et consignes

## Dispositif d’animation

Il y a un projet par groupe de 4.
On forme des paires, travail en peer programming

# Etapes et consignes

## Etape 1
### Temps : : 1H
### Modalités :
* Par groupe il choisissent une entreprise, secteur
* Ils devront faire 4 pages (une par personne) :
** actualités
** prodit
** équipe
** contact

Ils choisissent un site d’entreprise qui leur plaît.
Doivent définir leur fonctionnement git

Debrief par groupe devant tous -> présentation de 5 min de leur projet

### Consignes

20 min
Regrouper par groupe de 4 [veiller à une hétérogénéité]
Vous devez concevoir un site avec 4 pages :
* actualités
* produits
* équipe
* contact

Choisissez un site d’entreprise qui vend des produits et qui vous plaît.

Choisissez un nom d’entreprise, un secteur d’activités.

Répartissez-vous les pages.
-> un paperboard descriptif

15 min
1 debrief par groupe de 3 min

2/ Pensez à comment vous allez vous organiser :
projet
gestion du design
20 min
restit 2X4 = 10min

## Etape 2
Temps : ~4 ½ journées
Chacun fait une page avec le design choisi
-> faire une page avec le même design que le site choisi (en html, css)
merges réguliers dans une branche develop
Final : merge master

Debrief dans le groupe quand tout le site est ok
-> Quand tous les groupes sont ok, présentation de chaque site des difficultés rencontrées, 

## Etape 3
Temps : ~4 ½ journées
-> Animer la page avec du javascript
Choisir le type d’animation, sélectionner 2 pages entre produits, actualités et équipe

Le dev. se fait par 2 en peer programming : on inverse toutes les 2 heures?

Debrief dans le groupe
-> Quand tous les groupes sont ok, présentation de chaque site des difficultés rencontrées, 

## Etape 4
Temps : ~2 ½ journéess
-> Charger les éléments (actualités ou membres de l’équipe) depuis une bdd

voir git : https://gitlab.com/soulet.informatique/20190211-exercices-db

## Etape 5
Temps : 3 1/2 journées

Pré-requis : chaque repo git a un readme.md qui explique comment installer le site.

* En groupe on établit les critères à évaluer
** fonctionnel
** code
** design
** acessibilité
* Chaque groupe prend le git d'un autre groupe et doit fournir un fichier md avec (2h) :
Toutes les remarques sur le site réparti en n sections [vu à l'étape précédente]
* Chaque groupe reprend les remarques qui lui ont été faites et décide des modifs à apporter en prenant pour chaque remarque :
** Evaluer le temps
** Evaluer l'importance
** Objectif : la version finale doit être versée sur master à 12h
* Faire les correctifs (1/2 journée)
* Debrief général
** Présentation du site :
Présenter votre site 5 min

** Sur les technos : html,css,js,bdd,php : qu'est-ce que j'ai appris? quelle difficulté? Qu'est-ce qu'il faut que je bosse ?
** Sur le travail en groupe : qu'est-ce qui a marché ? qu'est-ce qui n'a pas marché ? Quels problèmes nous avons eu et comment l'avons-nous surmonté ?
** Sur le projet : qu'est-ce que le résultat suscite en vous ? Qu'est-ce que vous avez appris ? Qu'est-ce que vous souhaitez améliorer ?
** Sur la pédagogie : qu'est-ce que vous avez apprécié ? Qu'est-ce qui vous a manqué ?
** Gestion du projet
Qu'est-ce que j'ai appris sur le fonctionnement d'un site ? (lien entre les différentes parties, organisation, ...)

Préparation du debrief:
1H (10min x 5)
5 x 5 = 25 min
4 X 10 = 40 min



## Difficultés à anticiper
* Copie à l’identique d’un site?
* gestion du git, notamment de se mettre d’accord pour le merge
* utiliser php et bdd
* Travail de groupe

