Projet wordpress
================

Il n'y a pas eu de brief à proprement parlé. Introduction du module par Agnès Carton-Heurtebize, développeuse indépendante qui fait des sites wordpress.

# Atelier avec Agnès sur Wordpress
A partir de 4 angles, pendant 1h00, les apprenant-es préparent des questions pour agnès puis échange avec elle sur son métier


# Atelier avec le client
A ce stade, les apprenant-es ont installé un wordpress sur leur machine (mysql + wordpress de base)

14h00 - 15h00 : PRÉSENTATION ET ÉCHANGE COLLECTIF AVEC LE CLIENT
Présentation de chaque groupe + recueil des ressentis du client (5 x 5 mn)
Questions transverses en groupe (30 mn)

15h00 - 15h50 : REVUE CLIENT PAR GROUPE (10 mn par groupe)
Aspects positif et négatif
Pistes de travail

15h50 - 16h00 : DEBRIEF SUR LA SESSION PAR LE CLIENT

# Implémentation du site

# Echange avec le client
Présentation de 7 min de chaque projet avec ressentis globaux. Puis échanges individuels.

# Finalisation du site

# Présentation au client

