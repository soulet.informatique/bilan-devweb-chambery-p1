Brief Projet Chef d'oeuvre
==========================

Le projet chef d'oeuvre est un projet individuel qui se déroulera sur 5 semaines du 29/07 au 30/08.

Rappel : le projet chef d'oeuvre est un outil qui permet de valoriser les compétences acquises lors du passage du titre professionnel.

# Objectifs
Doit reprendre les principales compétences :
1 - Maquetter
2 - Web statique
3 - Web dynamique
5 - Créer une BDD
6 - Composants d'accès à la BDD
7 - Partie Back-end

# Description
Le projet devra en outre aborder de façon approfondie les compétences où l'apprenant-e n'est pas encore au niveau "Transposer" au démarrage du projet.

Avant le 05/07/2019, il est demandé à chaque apprenant-e de présenter une description succinte de son projet et une présentation par compétence qui fait le lien entre la compétence et la partie du projet concernée.