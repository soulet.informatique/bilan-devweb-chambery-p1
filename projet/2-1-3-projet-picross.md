Projet picross
==============

# Objectifs
* Découverte des éléments de base de l'algo (variables, conditions, opérateurs, boucles, ...)
* Compétence web dynamique : niveau 1
* Compétence maquettage : niveau 1

# Dispositif
* Travail par deux
* Etapes
** Etape 1 : définir la logique de l'implémentation
** Etape 2 : Faire l'aspect graphique 6X6
** Etape 3 : implémenter première feature : clic droit : valider, clic gauche mettre une croix
** Etape 4 : validation du picross
** Etape 6 : pouvoir importer des tableaux de taille variable en JSON
** Etape 5 : définir des fonctionnalités annexes : timer, level

# Support
"Titanic"
columns: [0,1,5,4,0]
rows: [1,2,2,3,2]
"Superman"
columns [[4],[7],[2,5],[1,2,1,3],[1,2,2,2],[1,2,2,2],[1,3,3],[2,6],[7],[4]]
rows:[[8],[3,3],[2,4,2],[2,7],[3,4],[5,2],[2,2,2],[2,7],[3,4],[5,2],[2,2,2],[2,2],[6],[4]