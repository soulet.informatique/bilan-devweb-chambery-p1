Brief projet "Thermique"
========================

Ce projet est fait en collaboration avec l'entreprise 3DNova, chaque apprenant-e ayant signé la convention de projet pédagogique (accessible dans le drive, projet 3DNova), il.elle s'engage à garder les données partagées par l'entreprise 3Dnova confidentielles. Ces données sont accessibles pour tou-te-s les apprenant-es dans le dossier "Documents" du dossier drive sus-cité.

Ce projet a pour but de créer une interface permettant de croiser les données de co-propriétaires, d'une co-propriété et des études thermiques envisagées en fournissant une aide à la décision individuelle sur l'opportunité d'engager des travaux de rénovation thermique.

# Description du projet
Pour la suite, nous diviserons le projet en 5 modules:

* 1. Base de données client (import) + admin formulaire
* 2. Gestion de compte, formulaire
* 3. Données immeuble + travaux + scénario
* 4. Gestion des subventions
* 5. Export par lot des scénarios

## Module 1 : les données des co-propriétaires
Les données sont issues de la co-propriété sous un format excel. Voir le fichier "exemple import initial BD.xlsx". La clef unique d'identification est le numéro de lot. Pour chaque lot, une interface admin permet de modifier les différents champs relatifs à l'import initial et au lot.

## Module 2 : la gestion de compte
Il s'agit de proposer une interface de gestion de compte par co-propriétaire (ou par lot) permettant de remplir le formulaire personnalisé. Voir "Questionnaire IF.pdf"

## Module 3 : les données liées au scénario
La totalité de ces éléments sont paramétrables depuis l'interface admin et permettent de croiser les données liées à l'immeuble, au diagnostic énergétique et de proposer les travaux envisagés avec leur coût. Voir document "Données techniques d'entrée.docx"

## Module 4 : subventions éligibles
Ce module sera réalisé de façon indépendante (bundle indépendant) qui proposera :
* une liste de paramètres d'entrée
* une sortie
* un paramétrage par item pour entrée le paramétrage de la subvention

## Module 5 : export des données par lot
Ce module permet à partir des données entrées par 1, 2, 3, 4 de proposer les coûts individuels pour chaque lot.

# Objectifs pédagogiques
Les savoirs attendus à l'issue du projet
* Prendre en main symfony
* Créer une application consistante à partir d'une demande client réelle
* Travailler en groupe de façon efficace
* Résoudre des problématiques client, trouver des solutions

Les compétences visées:
* Compétence 1, maquettage : niveau transposer
* Compétence 2, web statique : niveau transposer
* Compétence 3, web dynamique : niveau adapter/transposer
* Compétence 5, créer une base de donnée : niveau transposer
* Compétence 6, développer les composants d'accès aux données : niveau adapter/transposer
* Compétence 7, développer la partie back-end d'une application : niveau transposer

Les attendus :
Les 5 groupes devront avoir mis en place les modules indispensables dans un repo git avec :
* Module 1, 3 : ces deux modules devront être totalement finalisés
* Module 5 : un export du tableau avec le coût individuel pour chaque scénario

Le module 2 devra être a minima fonctionnel.
Le module 4 pourra être repris d'un autre groupe
Le module 5 finalisé consistera à reprendre un fichier odt pour remplacer les données marquées

# Modalités pédagogiques

Les temps de travail
* Semaine 1 : du 20/05 au 24/05
* Semaine 2 : du 03/06 au 07/06
* Semaine 3 : du 10/06 au 14/06
* Semaine 4 : du 17/06 au 21/06
* Semaine 5 : du 24/06 au 28/06

Les rencontres avec 3dNova
14/05, 27/05, 11/06, une présentation au cours de la dernière semaine

## Semaine 1 : travail préparatoire
Chaque groupe devra préparer les éléments suivants :
* un diagramme des classes pour représenter l'ensemble des entités du projet
* par module :
** quelles sont les technos qui seront utilisées et comment
** une description du fonctionnement de chaque module
** un schéma uml des use case
** les wireframes pour chaque cas d'utilisation
* Un plan de travail sur les 5 semaines, définir un chef de projet et un responsable pour chaque module
-> réunion de comparaison des diagrammes de classes le 23/05 à 9h

## Semaine 2 :
Travaux par binôme sur les modules 1/3

## Semaine 3 :
Finalisation des modules 1/3, début des modules 2/4

## Semaine 4 :
Finalisation des modules 2/4, mise en place du dernier module

## Semaine 5 :
Finalisation de l'application

Contraintes spécifiques :
* pas de css sur les 2 premières semaines (excepté pour menu et organisation de la page)
