Brief pour les formateurs
=========================

# Semaine du 20/05 au 24/05
* lundi : veille techno sur twig par Tommy
* mercredi : veille techno sur les formulaires par Nacime
* jeudi : veille techno sur FosUserBundle par Jordi

## Lundi
* 9h : veille techno
* 9h30 : envoi du brief, lecture + clarification
* 10h : travail par groupe pour définir le fonctionnement du travail dans le groupe sur les 5 semaines
* 11h : rendu du fonctionnement de chaque groupes
* 13h-17h -> travail sur le projet + entretiens individuels debrief projet portfolio et évaluation des compétences.

## Mardi
projet + opquast

## Mercredi
* Préparer des exercices sur le formulaire ? (Jérôme)
+ projet + finaliser les entretiens individuels sur le portfolio

## Jeudi
* Préparer exercices et réflexion sur la sécurité
* projet

## Vendredi
* matin : Bilan de mi-parcours
* après-midi : travail sur le projet
+ travail de préparation sur les questions à poser à Theo

# Semaine du 27/05 au 29/05
-> lundi : Theo revient à 9h
-> mardi, mercredi 2 veilles techno (?? - ??)
-> Brief projet Sylius (par Jérôme)

# Semaine du 03/06 au 07/06
exercices et veilles technos à définir


# A prévoir pour l'équipe pédago

## Symfony
voter, formulaire imbriqué, fixture, Introduction du test unitaire, webpack

## Mise en place sur le serveur
Voir avec 3dnova les modalités

## Mise en place du projet en AGPL v3
-> voir ce qu'il faut faire et comment gérer l'après

